package utils;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import model.Product;
import model.ProductName;
import service.implementation.ProductManager;

public class StringProductMapperTest {
	
	private static final String BREAD = "Bread";
	private static final String MILK = "Milk";
	private static final String APPLES = "Apples";
	private static final String SOMETHING2 = "Something2";
	private static final String SOMETHING1 = "Something1";
	
	private static List<Product> productList;
	private static Map<String, Product> availableProducts;
	
	@Before
    public void beforeEachMethod(){
		productList = new ArrayList<>();
		availableProducts = new HashMap<>();
		
		Product product = new Product();
        product.setName(ProductName.APPLES);
        product.setPrice(new BigDecimal(1.00));
        productList.add(product);
        availableProducts.put("APPLES", product);
        
        product = new Product();
        product.setName(ProductName.MILK);
        product.setPrice(new BigDecimal(1.30));
        productList.add(product);
        availableProducts.put("MILK", product);
        
        product = new Product();
        product.setName(ProductName.BREAD);
        product.setPrice(new BigDecimal(0.80));
        productList.add(product);
        availableProducts.put("BREAD", product);
        
        product = new Product();
        product.setName(ProductName.SOUP);
        product.setPrice(new BigDecimal(0.65));
        availableProducts.put("SOUP", product);

        ProductManager productManager = mock(ProductManager.class);
        when(productManager.getProductMap()).thenReturn(availableProducts);
        StringProductMapper.setProductManager(productManager);
	}
	
	@Test
	public void testMapStringsToProductsForEmptyProductList() throws Exception {
		productList = new ArrayList<>();
		
		List<String> itemList = new ArrayList<>();
        assertEquals(StringProductMapper.mapStringsToProducts(itemList), productList);
        
        itemList = new ArrayList<>(Arrays.asList(SOMETHING1));
        assertEquals(StringProductMapper.mapStringsToProducts(itemList), productList);
        
        itemList = new ArrayList<>(Arrays.asList(SOMETHING1, SOMETHING2));
        assertEquals(StringProductMapper.mapStringsToProducts(itemList), productList);
	}
	
	@Test
	public void testMapStringsToProducts() {
		List<String> itemList = new ArrayList<>(Arrays.asList(APPLES, MILK, BREAD));
        assertEquals(StringProductMapper.mapStringsToProducts(itemList), productList);
        
        itemList = new ArrayList<>(Arrays.asList(APPLES, MILK, BREAD, SOMETHING1));
        assertEquals(StringProductMapper.mapStringsToProducts(itemList), productList);
        
        itemList = new ArrayList<>(Arrays.asList(APPLES, MILK, BREAD, SOMETHING1, SOMETHING2));
        assertEquals(StringProductMapper.mapStringsToProducts(itemList), productList);
	}

}
