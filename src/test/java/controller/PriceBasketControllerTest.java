package controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Matchers;

import model.Product;
import model.ProductName;
import service.SpecialOffer;
import service.implementation.ApplesOffer;
import service.implementation.OffersManager;
import service.implementation.ProductManager;
import service.implementation.SoupWithBreadOffer;
import utils.StringProductMapper;

public class PriceBasketControllerTest {
	
	private static final String TOTALS_NO_OFFERS = 
			"Subtotal: �0.00\n" + 
			"(no offers available)\n" + 
			"Total: �0.00";
	private static final String TOTALS_APPLES_OFFER = 
			"Subtotal: �1.00\n" + 
			"Apples 10% off: -10p\n" + 
			"Total: 90p";
	private static final String TOTALS_SOUP_BRED_OFFER = 
			"Subtotal: �2.10\n" + 
			"For 2 soup tins 50% off bread loaf: -40p\n" + 
			"Total: �1.70";
	private static final String TOTALS_BOTH_OFFERS = 
			"Subtotal: �3.10\n" + 
			"For 2 soup tins 50% off bread loaf: -40p\n" + 
			"Apples 10% off: -10p\n" + 
			"Total: �2.60";
	
	private static final String FOR_2_SOUP_TINS_50_OFF_BREAD_LOAF = "For 2 soup tins 50% off bread loaf:";
	private static final String APPLES_10_OFF = "Apples 10% off:";
	private static final BigDecimal _10P = new BigDecimal(0.10).setScale(2, RoundingMode.FLOOR);
	private static final BigDecimal _40P = new BigDecimal(0.40).setScale(2, RoundingMode.FLOOR);
	private static final BigDecimal _00P = new BigDecimal(0.00).setScale(2, RoundingMode.FLOOR);
	
	private static PriceBasketController priceBasketController;
	private static List<SpecialOffer> offersList;
	private static Map<String, Product> availableProducts;
	
	@BeforeClass
	public static void beforeClass() {
		priceBasketController = new PriceBasketController();
	}
	
	@Before
	public void beforeEachMethod() {
		availableProducts = new HashMap<>();
		
		Product product = new Product();
        product.setName(ProductName.APPLES);
        product.setPrice(new BigDecimal(1.00));
        availableProducts.put("APPLES", product);
        
        product = new Product();
        product.setName(ProductName.MILK);
        product.setPrice(new BigDecimal(1.30));
        availableProducts.put("MILK", product);
        
        product = new Product();
        product.setName(ProductName.BREAD);
        product.setPrice(new BigDecimal(0.80));
        availableProducts.put("BREAD", product);
        
        product = new Product();
        product.setName(ProductName.SOUP);
        product.setPrice(new BigDecimal(0.65));
        availableProducts.put("SOUP", product);
        
        ProductManager productManager = mock(ProductManager.class);
        when(productManager.getProductMap()).thenReturn(availableProducts);
        StringProductMapper.setProductManager(productManager);
	}
	
	@Test
	public void testGetTotalsNoOffers() {
		offersList = new ArrayList<>();
		
		ApplesOffer ao = mock(ApplesOffer.class);
		when(ao.getOfferName()).thenReturn(APPLES_10_OFF);
		when(ao.getReducedAmount(Matchers.anyListOf(Product.class))).thenReturn(_00P);
		offersList.add(ao);
		
		SoupWithBreadOffer sbo = mock(SoupWithBreadOffer.class);
		when(sbo.getOfferName()).thenReturn(FOR_2_SOUP_TINS_50_OFF_BREAD_LOAF);
		when(sbo.getReducedAmount(Matchers.anyListOf(Product.class))).thenReturn(_00P);
		offersList.add(sbo);
		
		OffersManager offersManager = mock(OffersManager.class);
        when(offersManager.getOffersList()).thenReturn(offersList);
        priceBasketController.setOffersManager(offersManager);
		
		List<String> items = new ArrayList<>();
		assertEquals(TOTALS_NO_OFFERS, priceBasketController.getTotals(items));
	}
	
	@Test
	public void testGetTotalsApplesOffer() {
		offersList = new ArrayList<>();
		
		ApplesOffer ao = mock(ApplesOffer.class);
		when(ao.getOfferName()).thenReturn(APPLES_10_OFF);
		when(ao.getReducedAmount(Matchers.anyListOf(Product.class))).thenReturn(_10P);
		offersList.add(ao);
		
		SoupWithBreadOffer sbo = mock(SoupWithBreadOffer.class);
		when(sbo.getOfferName()).thenReturn(FOR_2_SOUP_TINS_50_OFF_BREAD_LOAF);
		when(sbo.getReducedAmount(Matchers.anyListOf(Product.class))).thenReturn(_00P);
		offersList.add(sbo);
		
		OffersManager offersManager = mock(OffersManager.class);
        when(offersManager.getOffersList()).thenReturn(offersList);
        priceBasketController.setOffersManager(offersManager);
		
		List<String> items = new ArrayList<>(Arrays.asList("Apples"));
		assertEquals(TOTALS_APPLES_OFFER, priceBasketController.getTotals(items));
	}
	
	@Test
	public void testGetTotalsSoupWithBreadOffer() {
		offersList = new ArrayList<>();
		
		ApplesOffer ao = mock(ApplesOffer.class);
		when(ao.getOfferName()).thenReturn(APPLES_10_OFF);
		when(ao.getReducedAmount(Matchers.anyListOf(Product.class))).thenReturn(_00P);
		offersList.add(ao);
		
		SoupWithBreadOffer sbo = mock(SoupWithBreadOffer.class);
		when(sbo.getOfferName()).thenReturn(FOR_2_SOUP_TINS_50_OFF_BREAD_LOAF);
		when(sbo.getReducedAmount(Matchers.anyListOf(Product.class))).thenReturn(_40P);
		offersList.add(sbo);
		
		OffersManager offersManager = mock(OffersManager.class);
        when(offersManager.getOffersList()).thenReturn(offersList);
        priceBasketController.setOffersManager(offersManager);
		
		List<String> items = new ArrayList<>(Arrays.asList("Soup", "Soup", "Bread"));
		assertEquals(TOTALS_SOUP_BRED_OFFER, priceBasketController.getTotals(items));
	}
	
	@Test
	public void testGetTotalsBothOffers() {
		offersList = new ArrayList<>();
		
		ApplesOffer ao = mock(ApplesOffer.class);
		when(ao.getOfferName()).thenReturn(APPLES_10_OFF);
		when(ao.getReducedAmount(Matchers.anyListOf(Product.class))).thenReturn(_10P);
		offersList.add(ao);
		
		SoupWithBreadOffer sbo = mock(SoupWithBreadOffer.class);
		when(sbo.getOfferName()).thenReturn(FOR_2_SOUP_TINS_50_OFF_BREAD_LOAF);
		when(sbo.getReducedAmount(Matchers.anyListOf(Product.class))).thenReturn(_40P);
		offersList.add(sbo);
		
		OffersManager offersManager = mock(OffersManager.class);
        when(offersManager.getOffersList()).thenReturn(offersList);
        priceBasketController.setOffersManager(offersManager);
		
		List<String> items = new ArrayList<>(Arrays.asList("Apples", "Soup", "Soup", "Bread"));
		assertEquals(TOTALS_BOTH_OFFERS, priceBasketController.getTotals(items));
	}
}
