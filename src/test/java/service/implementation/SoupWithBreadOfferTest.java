package service.implementation;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import model.Product;
import model.ProductName;

public class SoupWithBreadOfferTest {
	
	private static final String OFFER_NAME = "For 2 soup tins 50% off bread loaf:";
	
	private static SoupWithBreadOffer soupWithBreadOffer;
	private static Map<String, Product> availableProducts;
	private static Product apple;
	private static Product milk;
	private static Product bread;
	private static Product soup;
	
	@BeforeClass
	public static void beforeClass() {
		soupWithBreadOffer = new SoupWithBreadOffer();
		soupWithBreadOffer.setOfferName(OFFER_NAME);
		availableProducts = new HashMap<>();
		
		Product product = new Product();
        product.setName(ProductName.APPLES);
        product.setPrice(new BigDecimal(1.00));
        availableProducts.put("APPLES", product);
        apple = product;
        
        product = new Product();
        product.setName(ProductName.MILK);
        product.setPrice(new BigDecimal(1.30));
        availableProducts.put("MILK", product);
        milk = product;
        
        product = new Product();
        product.setName(ProductName.BREAD);
        product.setPrice(new BigDecimal(0.80));
        availableProducts.put("BREAD", product);
        bread = product;
        
        product = new Product();
        product.setName(ProductName.SOUP);
        product.setPrice(new BigDecimal(0.65));
        availableProducts.put("SOUP", product);
        soup = product;
        
        ProductManager productManager = mock(ProductManager.class);
        when(productManager.getProductMap()).thenReturn(availableProducts);
        soupWithBreadOffer.setProductManager(productManager);
	}
	
	@Test
	public void testGetOfferName() {
		assertEquals(OFFER_NAME, soupWithBreadOffer.getOfferName());
	}
	
	@Test
	public void testGetReducedAmount() {
		List<Product> productList = new ArrayList<>();
		BigDecimal amount = new BigDecimal(0.00).setScale(2);
		assertEquals(amount, soupWithBreadOffer.getReducedAmount(productList));
		
		productList = new ArrayList<>(Arrays.asList(apple, milk));
		assertEquals(amount, soupWithBreadOffer.getReducedAmount(productList));
		
		productList = new ArrayList<>(Arrays.asList(apple, milk, bread));
		assertEquals(amount, soupWithBreadOffer.getReducedAmount(productList));
		
		productList = new ArrayList<>(Arrays.asList(soup, apple, milk, bread));
		assertEquals(amount, soupWithBreadOffer.getReducedAmount(productList));
		
		productList = new ArrayList<>(Arrays.asList(soup, soup, apple, milk, bread));
		amount = new BigDecimal(0.40).setScale(2, RoundingMode.FLOOR);
		assertEquals(amount, soupWithBreadOffer.getReducedAmount(productList));
		
		productList = new ArrayList<>(Arrays.asList(soup, soup, apple, milk, bread, bread));
		assertEquals(amount, soupWithBreadOffer.getReducedAmount(productList));
		
		productList = new ArrayList<>(Arrays.asList(soup, soup, soup, apple, milk, bread));
		assertEquals(amount, soupWithBreadOffer.getReducedAmount(productList));
		
		productList = new ArrayList<>(Arrays.asList(soup, soup, soup, soup, apple, milk, bread));
		assertEquals(amount, soupWithBreadOffer.getReducedAmount(productList));
		
		productList = new ArrayList<>(Arrays.asList(soup, soup, soup, soup, apple, milk, bread, bread));
		amount = new BigDecimal(0.80).setScale(2, RoundingMode.FLOOR);
		assertEquals(amount, soupWithBreadOffer.getReducedAmount(productList));
	}
}
