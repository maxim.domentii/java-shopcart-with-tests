package service.implementation;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import service.SpecialOffer;

public class OffersManagerTest {
	
	private static final String OFFER_NAME = "Apples 10% off:";
	
	private static OffersManager offersManager;
	private static List<SpecialOffer> offersList;
	
	@BeforeClass
	public static void beforeClass() {
		offersManager = new OffersManager();
		offersList = new ArrayList<>();
		
		ApplesOffer ao = new ApplesOffer();
		ao.setOfferName(OFFER_NAME);
		offersList.add(ao);
        
		offersManager.setOffersList(offersList);
	}
	
	@Test
	public void testGetOffersList() {
		assertEquals(offersList.size(), offersManager.getOffersList().size());
		
		SpecialOffer offer = offersManager.getOffersList().get(0);
		assertEquals(OFFER_NAME, offer.getOfferName());
	}
}
