package service.implementation;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import model.Product;
import model.ProductName;

public class ProductManagerTest {
	
	private static ProductManager productManager;
	private static Map<String, Product> availableProducts;
	
	@BeforeClass
	public static void beforeClass() {
		productManager = new ProductManager();
		availableProducts = new HashMap<>();
		
		Product product = new Product();
        product.setName(ProductName.APPLES);
        product.setPrice(new BigDecimal(1.00));
        availableProducts.put("APPLES", product);
        
        productManager.setProductMap(availableProducts);
	}
	
	@Test
	public void testGetProductMap() {
		assertEquals(availableProducts.size(), productManager.getProductMap().size());
		
		Product apples = productManager.getProductMap().get("APPLES");
		assertEquals(ProductName.APPLES, apples.getName());
		assertEquals(new BigDecimal(1.00), apples.getPrice());
	}

}
