package service.implementation;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import model.Product;
import model.ProductName;

public class ApplesOfferTest {
	
	private static final int PERCENTAGE_TO_REDUCE = 10;
	private static final String OFFER_NAME = "Apples 10% off:";
	
	private static ApplesOffer applesOffer;
	private static Map<String, Product> availableProducts;
	private static Product apple;
	private static Product milk;
	private static Product bread;
	private static Product soup;
	
	@BeforeClass
	public static void beforeClass() {
		applesOffer = new ApplesOffer();
		applesOffer.setPercentageToReduce(PERCENTAGE_TO_REDUCE);
		applesOffer.setOfferName(OFFER_NAME);
		availableProducts = new HashMap<>();
		
		Product product = new Product();
        product.setName(ProductName.APPLES);
        product.setPrice(new BigDecimal(1.00));
        availableProducts.put("APPLES", product);
        apple = product;
        
        product = new Product();
        product.setName(ProductName.MILK);
        product.setPrice(new BigDecimal(1.30));
        availableProducts.put("MILK", product);
        milk = product;
        
        product = new Product();
        product.setName(ProductName.BREAD);
        product.setPrice(new BigDecimal(0.80));
        availableProducts.put("BREAD", product);
        bread = product;
        
        product = new Product();
        product.setName(ProductName.SOUP);
        product.setPrice(new BigDecimal(0.65));
        availableProducts.put("SOUP", product);
        soup = product;
        
        ProductManager productManager = mock(ProductManager.class);
        when(productManager.getProductMap()).thenReturn(availableProducts);
        applesOffer.setProductManager(productManager);
	}
	
	@Test
	public void testGetOfferName() {
		assertEquals(OFFER_NAME, applesOffer.getOfferName());
	}
	
	@Test
	public void testGetReducedAmount() {
		List<Product> productList = new ArrayList<>();
		BigDecimal amount = new BigDecimal(0.00).setScale(2);
		assertEquals(amount, applesOffer.getReducedAmount(productList));
		
		productList = new ArrayList<>(Arrays.asList(soup, bread, milk));
		assertEquals(amount, applesOffer.getReducedAmount(productList));
		
		productList = new ArrayList<>(Arrays.asList(apple));
		amount = new BigDecimal(0.10).setScale(2, RoundingMode.FLOOR);
		assertEquals(amount, applesOffer.getReducedAmount(productList));
		
		productList = new ArrayList<>(Arrays.asList(apple, soup, bread, milk));
		assertEquals(amount, applesOffer.getReducedAmount(productList));
		
		productList = new ArrayList<>(Arrays.asList(apple, apple));
		amount = new BigDecimal(0.20).setScale(2, RoundingMode.FLOOR);
		assertEquals(amount, applesOffer.getReducedAmount(productList));
		
		productList = new ArrayList<>(Arrays.asList(apple, soup, bread, milk, apple));
		assertEquals(amount, applesOffer.getReducedAmount(productList));
	}

}
