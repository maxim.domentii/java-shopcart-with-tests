package model;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.BeforeClass;
import org.junit.Test;

public class ProductTest{
	
	private static Product product;

	@BeforeClass
    public static void beforeClass(){
        product = new Product();
    }

	@Test
    public void testSetAndGetName() {
        ProductName testName = ProductName.APPLES;
        assertNull(product.getName());
        product.setName(testName);
        assertEquals(testName, product.getName());
    }
    
	@Test
    public void testSetAndGetPrice() {
    	BigDecimal testPrice = new BigDecimal(1.00);
    	assertNull(product.getPrice());
    	product.setPrice(testPrice);
    	assertEquals(testPrice, product.getPrice());
    }

}
