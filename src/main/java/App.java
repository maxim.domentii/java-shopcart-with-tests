import java.util.Arrays;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import controller.PriceBasketController;

/**
 * Main class with a main method as a starting execution point of the program.
 * 
 * @author maxim.domentii
 *
 */
public class App {

	/**
	 * Main method which is used by JVM to start execution of the program.
	 * This method will display in console the expected output. 
	 * 
	 * @param args is an array of string representing the items given as arguments in command line - input of the program.
	 */
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/beans.xml");
    	
		PriceBasketController pbc = (PriceBasketController) context.getBean("priceBasketController");
		
    	List<String> items = Arrays.asList(args);
		System.out.println(pbc.getTotals(items));
	}

}
