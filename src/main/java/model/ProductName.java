package model;

/**
 * ProductName is an enum class where I configured the possible names that products can take.
 * If you want to add a new product then you have only to add a new element in this enum class
 * and all logic related to product name will not be affected and will not be required to do
 * any other changes.
 * 
 * @author maxim.domentii
 *
 */
public enum ProductName {
	APPLES, MILK, BREAD, SOUP; 
}
