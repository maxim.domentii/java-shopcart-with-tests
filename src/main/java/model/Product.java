package model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Product class represent items to be processed which have two fields: name and price.
 * Name is of type {@link ProductName}.
 * Price is of type {@link BigDecimal}. I have decided to use BigDecimal type because 
 * even though money is not going to be large enough to need the precision of 
 * BigDecimal in 99% of use cases, it is often considered best practice to use BigDecimal 
 * because the control of rounding is in the software which avoids the risk that the developer
 * will make a mistake in handling rounding.
 * This class implements {@link Serializable} interface in order to be possible to serialize
 * objects of type {@link Product}. For example if we will decide to introduce a persistence layer.
 * 
 * @author maxim.domentii
 *
 */
public class Product implements Serializable {
	
	private static final long serialVersionUID = -7976836497692103420L;
	
	private ProductName name;
	private BigDecimal price;
	
	public ProductName getName() {
		return name;
	}
	public void setName(ProductName name) {
		this.name = name;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
}
