package controller;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import model.Product;
import service.IOffersManager;
import service.SpecialOffer;
import utils.StringProductMapper;

/**
 * PriceBasketController class control the logic of app flow.
 * It will receive a list of items to be processed, will compute the subtotal,
 * will apply the offers if it is the case using IOffersManager which is injected in the controller
 * and will compute the total amount which is reduced amount substracted form subtotal.
 * Finally, it will build the output as a string.
 * 
 * @author maxim.domentii
 *
 */
public class PriceBasketController {
	
	private static final String NEW_LINE = "\n";
	private static final String NO_OFFERS_REDUCTION = "(no offers available)";
	private static final String POUNDS = "�";
	private static final String COIN = "p";
	
	private IOffersManager offersManager;
	
	public void setOffersManager(IOffersManager offersManager) {
		this.offersManager = offersManager;
	}
	
	/**
	 * Receive a list of items, map them to a list of products with name and price,
	 * process them and return a string representing the total - expected output of the program.
	 * 
	 * @param items list of string representing the products to be processed.
	 * @return a string representing the total - expected output of the program.
	 */
	public String getTotals(List<String> items) {
		List<Product> productList = mapItemsToProducts(items);
		
		String totals  = getTotalAsString(productList);
		
		return totals;
	}
	
	/**
	 * Receive a list of products, compute the subtotal, apply offers, compute total and
	 * build output as a string.
	 * 
	 * @param productList for which to determine the total
	 * @return a string representing the total - expected output of the program.
	 */
	private String getTotalAsString(List<Product> productList) {
		StringBuilder sb = new StringBuilder();
		
		BigDecimal subTotal = getSubTotal(productList).setScale(2, RoundingMode.FLOOR);
		if (subTotal.compareTo(BigDecimal.ONE) >= 0 || subTotal.compareTo(BigDecimal.ZERO) == 0) {
			sb.append("Subtotal: " + POUNDS + subTotal + NEW_LINE);
		} else {
			sb.append("Subtotal: " + getDecimal(subTotal) + COIN + NEW_LINE);
		}
		
		Map<String, BigDecimal> map = getAmountToBeReducedMap(productList);
		if (map.size()==0) {
			sb.append(NO_OFFERS_REDUCTION + NEW_LINE);
		} else {
			for (Entry<String, BigDecimal> entry : map.entrySet()) {
				BigDecimal value = entry.getValue().setScale(2, RoundingMode.FLOOR);
				String key = entry.getKey();
				if (value.compareTo(BigDecimal.ONE) >= 0 || value.compareTo(BigDecimal.ZERO) == 0) {
					sb.append(key + " " + POUNDS + value + NEW_LINE);
				} else {
					sb.append(key + " -" + getDecimal(value) + COIN + NEW_LINE);
				}
			}
		}
		
		BigDecimal total = getTotal(subTotal, map).setScale(2, RoundingMode.FLOOR);
		if (total.compareTo(BigDecimal.ONE) >= 0 || total.compareTo(BigDecimal.ZERO) == 0) {
			sb.append("Total: " + POUNDS + total);
		} else {
			sb.append("Total: " + getDecimal(total) + COIN);
		}
		
		return sb.toString();
	}
	
	/**
	 * In case when amount is < 1, get from BigDecimal the decimal part to be displayed as coins.
	 * 
	 * @param bd BigDecimal number to be processed.
	 * @return the decimal part of given number.
	 */
	private BigInteger getDecimal(BigDecimal bd) {
		return bd.remainder(BigDecimal.ONE).movePointRight(bd.scale()).abs().toBigInteger();
	}
	
	/**
	 * Call utility method from {@link StringProductMapper} utility class to map a list of string
	 * to a list of products.
	 * 
	 * @param items list of strings to be map as products.
	 * @return list of products representing mapped given items as string.
	 */
	private List<Product> mapItemsToProducts(List<String> items){
		List<Product> productList = StringProductMapper.mapStringsToProducts(items);
		return productList;
	}
	
	/**
	 * Compute the subtotal of a given list of products.
	 * 
	 * @param productList products for which to compute the subtotal.
	 * @return the subtotal of the given list of products.
	 */
	private BigDecimal getSubTotal(List<Product> productList) {
		BigDecimal subtotal = BigDecimal.ZERO;
		for (Product p : productList) {
			subtotal = subtotal.add(p.getPrice());
		}
		return subtotal;
	}
	
	/**
	 * Compute the total by substracting the amount to be reduced because of offers from subtotal.
	 * 
	 * @param subtotal of the products
	 * @param map of offers which was applied and corresponding amount to be reduced from subtotal.
	 * @return the total of the products.
	 */
	private BigDecimal getTotal(BigDecimal subtotal, Map<String, BigDecimal> map) {
		BigDecimal total = subtotal;
		for (BigDecimal amount : map.values()) {
			total = total.subtract(amount);
		}
		return total;
	}
	
	/**
	 * Apply offers and build a map with offers which was applied and corresponding 
	 * amount to be reduced from subtotal.
	 *  
	 * @param productList on which to apply the offers
	 * @return map with offers which was applied and corresponding amount to be reduced from subtotal.
	 */
	private Map<String, BigDecimal> getAmountToBeReducedMap(List<Product> productList) {
		Map<String, BigDecimal> map = new HashMap<String, BigDecimal>();
		
		List<SpecialOffer> offers = offersManager.getOffersList();
		for (SpecialOffer offer : offers) {
			BigDecimal reducedAmount = offer.getReducedAmount(productList);
			if (reducedAmount.compareTo(BigDecimal.ZERO) > 0) {
				map.put(offer.getOfferName(), reducedAmount);
			}
		}
		
		return map;
	}

}
