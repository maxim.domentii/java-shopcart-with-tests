package utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import model.Product;
import service.IProductManager;

/**
 * StringProductMapper is a utility class that has a static method which map items given
 * in command line to a list of products with specific product details.
 * 
 * @author maxim.domentii
 *
 */
public class StringProductMapper {
	
	private static final Log LOGGER = LogFactory.getLog(StringProductMapper.class);
	
	private static IProductManager productManager;
	
	public static void setProductManager(IProductManager pm) {
		productManager = pm;
	}
	
	/**
	 * Map a list of string to a list of products.
	 * If a given item can be found in the available products then a error will be logged and
	 * this product will not be added in the shop cart.
	 * 
	 * @param items list of strings to be map as products.
	 * @return list of products representing mapped given items as string.
	 */
	public static List<Product> mapStringsToProducts(List<String> items){
		
		List<Product> productList = new ArrayList<Product>();
		
		Map<String, Product> availableProducts = productManager.getProductMap();
		for (String item : items) {
			if (availableProducts.containsKey(item.toUpperCase())) {
				productList.add(availableProducts.get(item.toUpperCase()));
			} else {
				LOGGER.error("No price available for product " + item + ". This item will not be added in the cart.");
			}
		}
		
		return productList;
	}

}
