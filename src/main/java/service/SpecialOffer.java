package service;

import java.math.BigDecimal;
import java.util.List;

import model.Product;

/**
 * SpecialOffer is an interface that is used to implement possible offers.
 * It has two method: getReducedAmount() and getOfferName().
 * getReducedAmount() take a list of products and check if a reduction should be applied
 * and return the total amount which have to be substracted from subtotal related to this offer.
 * getOfferName() return a string which will be displayed on output specifying for what reduction 
 * was applied.
 * 
 * @author maxim.domentii
 *
 */
public interface SpecialOffer {
	
	/**
	 * Take a list of products and check if a reduction should be applied and return the total amount 
	 * which have to be substracted from subtotal related to this offer.
	 * 
	 * @param productList a list of products.
	 * @return the total amount which have to be substracted from subtotal related to this offer.
	 */
	BigDecimal getReducedAmount(List<Product> productList);
	
	/**
	 * @return a string which will be displayed on output specifying for what reduction was applied.
	 */
	String getOfferName();

}
