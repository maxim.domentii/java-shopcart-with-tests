package service;

import java.util.List;

/**
 * IOffersManager is an interface used to manage the implemented offers.
 * Offers which are applied at the moment are injected as a list to this interface in the
 * config file beans.xml. If you want to exclude one of them or to add another one then you can
 * easily to do that through config file.
 * 
 * @author maxim.domentii
 *
 */
public interface IOffersManager {
	
	/**
	 * @return a list of available offers injected as a list to this interface in the config file beans.xml.
	 */
	List<SpecialOffer> getOffersList();

}
