package service.implementation;

import java.util.List;

import service.IOffersManager;
import service.SpecialOffer;

/**
 * OffersManager class implements {@link IOffersManager} interface which is giving access to offers.
 * 
 * @author maxim.domentii
 *
 */
public class OffersManager implements IOffersManager {
	
	private List<SpecialOffer> offersList;

	public List<SpecialOffer> getOffersList() {
		return offersList;
	}

	public void setOffersList(List<SpecialOffer> offersList) {
		this.offersList = offersList;
	}

}
