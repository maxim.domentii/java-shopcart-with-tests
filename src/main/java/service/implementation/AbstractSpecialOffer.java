package service.implementation;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import model.Product;
import service.IProductManager;
import service.SpecialOffer;

/**
 * AbstractSpecialOffer is an abstract class which implements {@link SpecialOffer} interface
 * and has a common logic for implemented offers.
 * 
 * @author maxim.domentii
 *
 */
public abstract class AbstractSpecialOffer implements SpecialOffer {
	
	protected final Log LOGGER = LogFactory.getLog(getClass());
	
	protected IProductManager productManager;
	protected String offerName;
	
	public void setProductManager(IProductManager productManager) {
		this.productManager = productManager;
	}

	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}
	
	public String getOfferName() {
		return offerName;
	}

	public BigDecimal getReducedAmount(List<Product> productList) {
		return applyOffer(productList);
	}

	/**
	 * Custom logic specific for each offer which check over a list of products
	 * if any reduction has to be applied and return the total amount for this list
	 * which has to be reduced from subtotal.
	 *  
	 * @param productList a list of products.
	 * @return the total amount for this list which has to be reduced from subtotal.
	 */
	protected abstract BigDecimal applyOffer(List<Product> productList);
}
