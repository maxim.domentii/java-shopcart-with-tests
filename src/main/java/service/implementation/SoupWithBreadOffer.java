package service.implementation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;

import model.Product;
import model.ProductName;

/**
 * SoupWithBreadOffer class extends {@link AbstractSpecialOffer} and implement offer related to 50% off
 * each bread loaf for each two tins of soup. I have decided to not allow configuration for
 * no of soups required to get reduction for bread and also the percentage of bread to
 * be reduced because it is a very custom offer and can be confusing for client to configure it.
 * If you decide for example to modify it it's better to implement a new one. 
 * 
 * @author maxim.domentii
 *
 */
public class SoupWithBreadOffer extends AbstractSpecialOffer {

	private static final int _50 = 50;

	@Override
	protected BigDecimal applyOffer(List<Product> productList) {
		int soupTinsCount = 0;
		for (Product product : productList) {
			if (ProductName.SOUP.equals(product.getName())) {
				soupTinsCount++; 
			}
		}
		if (soupTinsCount == 0) {
			return new BigDecimal(0.00).setScale(2);
		}
		
		int breadLoafsCount = 0;
		for (Product product : productList) {
			if (ProductName.BREAD.equals(product.getName())) {
				breadLoafsCount++; 
			}
		}
		if (breadLoafsCount == 0) {
			return new BigDecimal(0.00).setScale(2);
		}
		
		BigDecimal toReducedAmount = new BigDecimal(0.00).setScale(2);
		for(int i=0; i<soupTinsCount/2; i++) {
			if (breadLoafsCount > 0) {
				toReducedAmount = toReducedAmount.add(getReducedAmountOfOneBreadLoaf());
			} else {
				break;
			}
			breadLoafsCount--;
		}
		
		return toReducedAmount;
		
	}
	
	/**
	 * Compute the amount to be reduced for each loaf of bread according to specific of this offer.
	 * 
	 * @return the amount to be reduced for each loaf of bread.
	 */
	private BigDecimal getReducedAmountOfOneBreadLoaf() {
		Map<String, Product> productMap = productManager.getProductMap();
		
		BigDecimal breadLoafAmount = null;
		if (productMap.containsKey(ProductName.BREAD.name())) {
			breadLoafAmount = productMap.get(ProductName.BREAD.name()).getPrice();
		}
		
		BigDecimal reducedLoafAmount = BigDecimal.ZERO;
		try {
			reducedLoafAmount =  breadLoafAmount.divide(new BigDecimal(100)).multiply(new BigDecimal(_50)).setScale(2, RoundingMode.FLOOR);
		} catch (NullPointerException ex) {
			LOGGER.error("NullPointerException: Can't get Bread loaf price!");
			throw new NullPointerException("Can't get Bread loaf price!");
		}
		
		return reducedLoafAmount;
	}

}
