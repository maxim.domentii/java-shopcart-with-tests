package service.implementation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;

import model.Product;
import model.ProductName;

/**
 * ApplesOffer class  expends {@link AbstractSpecialOffer} and implement offer related to 10% off 
 * each apples bag. It has a parameter which represent the percentage to be reduced and 
 * can be configured from a configuration file (beans.xml in this case).
 * 
 * @author maxim.domentii
 *
 */
public class ApplesOffer extends AbstractSpecialOffer {
	
	private int percentageToReduce;
	
	public void setPercentageToReduce(int  percentageToReduce) {
		this.percentageToReduce = percentageToReduce;
	}
	
	protected BigDecimal applyOffer(List<Product> productList) {		
		int appleBagsCount = 0;
		for (Product product : productList) {
			if (ProductName.APPLES.equals(product.getName())) {
				appleBagsCount++; 
			}
		}
		
		BigDecimal toReducedAmount = getReducedAmountOfOneBag().multiply(new BigDecimal(appleBagsCount));
		return toReducedAmount;
	}
	
	/**
	 * Compute the amount to be reduced for each bag of apples according to specific of this offer.
	 * 
	 * @return the amount to be reduced for each bag of apples.
	 */
	private BigDecimal getReducedAmountOfOneBag() {
		Map<String, Product> productMap = productManager.getProductMap();
		
		BigDecimal appleBagAmount = null;
		if (productMap.containsKey(ProductName.APPLES.name())) {
			appleBagAmount = productMap.get(ProductName.APPLES.name()).getPrice();
		}
		
		BigDecimal reducedBagAmount = BigDecimal.ZERO;
		try {
			reducedBagAmount =  appleBagAmount.divide(new BigDecimal(100)).multiply(new BigDecimal(percentageToReduce)).setScale(2, RoundingMode.FLOOR);
		} catch (NullPointerException ex) {
			LOGGER.error("NullPointerException: Can't get Apple bag price!");
			throw new NullPointerException("Can't get Apple bag price!");
		}
		
		return reducedBagAmount;
	}

}
