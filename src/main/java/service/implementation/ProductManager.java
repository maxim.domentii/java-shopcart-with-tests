package service.implementation;

import java.util.Map;

import model.Product;
import service.IProductManager;

/**
 * ProductManager class implements {@link IProductManager} interface which is giving access to products.
 * 
 * @author maxim.domentii
 *
 */
public class ProductManager implements IProductManager {
	
	private Map<String, Product> productMap;
	
	public void setProductMap(Map<String, Product> productMap) {
		this.productMap = productMap;
	}
	
	public Map<String, Product> getProductMap(){
		return this.productMap;
	}

}
