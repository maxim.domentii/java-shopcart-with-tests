package service;

import java.util.Map;

import model.Product;

/**
 * IProductManager is an interface that is used to manage the available products in the app.
 * It has an method getProductMap() which return a map of available products where the key 
 * is the product identifier (name) and value is a specific {@link Product}.
 * Because for now we have only very few products I have decide to configure them in a config file
 * (beans.xml) in this case and inject them as a map in implementation of the IProductManager
 * interface. If we decide later to introduce a persistence layer than we can easily to implement
 * a DAO interface and inject it in ProductManager in order to have access to application products.
 * 
 * @author maxim.domentii
 *
 */
public interface IProductManager {
	
	/**
	 * @return a map of available products where the key is the product identifier (name) 
	 * and value is a specific {@link Product}.
	 */
	Map<String, Product> getProductMap();
	
}
