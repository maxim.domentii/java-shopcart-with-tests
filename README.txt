Java Shop-cart with JUnit
=========================

Technologies:
-------------
- Java 7
- junit and org.mockito for unit test
- commons-logging to log eventualy catched exeptions
- org.springframework to manage aplication context, beans configurations and dependency injection

Implementation:
---------------

-> Model enum ProductName

ProductName is an enum class where I configured the possible names that products can take.
If you want to add a new product then you have only to add a new element in this enum class
and all logic related to product name will not be affected and will not be required to do
any other changes.

-> Model class Product

Product class represent items to be processed which have two fields: name and price.
Name is of type ProductName.
Price is of type BigDecimal. I have decided to use BigDecimal type because 
even though money is not going to be large enough to need the precision of 
BigDecimal in 99% of use cases, it is often considered best practice to use BigDecimal 
because the control of rounding is in the software which avoids the risk that the developer
will make a mistake in handling rounding.
This class implements Serializable interface in order to be possible to serialize
objects of type Product. For example if we will decide to introduce a persistence layer.

-> Service interface SpecialOffer

SpecialOffer is an interface that is used to implement possible offers.
It has two method: getReducedAmount() and getOfferName().
getReducedAmount() take a list of products and check if a reduction should be applied
and return the total amount which have to be substracted from subtotal related to this offer.
getOfferName() return a string which will be displayed on output specifying for what reduction 
was applied.

-> Service abstract class AbstractSpecialOffer

AbstractSpecialOffer is an abstract class which implements SpecialOffer interface
and has a common logic for implemented offers.

-> Service class ApplesOffer

ApplesOffer class  extends AbstractSpecialOffer and implement offer related to 10% off 
each apples bag. It has a parameter which represent the percentage to be reduced and 
can be configured from a configuration file (beans.xml in this case).

-> Service class SoupWithBreadOffer

SoupWithBreadOffer class extends AbstractSpecialOffer and implement offer related to 50% off
each bread loaf for each two tins of soup. I have decided to not allow configuration for
no of soups required to get reduction for bread and also the percentage of bread to
be reduced because it is a very custom offer and can be confusing for client to configure it.
If you decide for example to modify it it's better to implement a new one. 

-> Service interface IProductManager

IProductManager is an interface that is used to manage the available products in the app.
It has an method getProductMap() which return a map of available products where the key 
is the product identifier (name) and value is a specific product.
Because for now we have only very few products I have decide to configure them in a config file
(beans.xml) in this case and inject them as a map in implementation of the IProductManager
interface. If we decide later to introduce a persistence layer than we can easily to implement
a DAO interface and inject it in ProductManager in order to have accesss to application products.

-> Service impl ProductManager

ProductManager class implements IProductManager interface which is giving access to products.

-> Service interface IOffersManager

IOffersManager is an interface used to manage the implemented offers.
Offers which are applied at the moment are injected as a list to this interface in the
config file beans.xml. If you want to exclude one of them or to add another one then you can
easily to do that through config file.

-> Service impl OffersManager implements IOffersManager

OffersManager class implements IOffersManager interface which is giving access to offers.

-> StringProductMapper

StringProductMapper is a utility class that has a static method which map items given
in command line to a list of products with specific product details.

-> PriceBasketController

PriceBasketController class control the logic of app flow.
It will receive a list of items to be processed, will compute the subtotal,
will apply the offers if it is the case using IOffersManager which is injected in the controller
and will compute the total amount which is reduced amount substracted form subtotal.
Finally, it will build the output as a string.

Requirements:
-------------
Apache Maven 3.X.X
JDK 1.7

Testing, Building and Runing
----------------------------

Unzip PriceBasket.zip arhive.

IN CMD:

Go in projetc root folder and ...

 - To run unit tests: mvn clean test
 - To build source packages: mvn clean package
 - To execute with maven: mvn exec:java -Dexec.mainClass="App" -Dexec.args="item1 item2 item3 …"

IN ECLIPSE:

Import as a maven project and create folowing maven run config ...

 - With goal "clean test" to run unit tests
 - With goal "clean package" to build source packages
 - With goal "exec:java" and parameter/values pairs mainClass/App, exec.args/item1 item2 item3 … to execute with maven
